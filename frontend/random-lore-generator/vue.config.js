const AssetsPlugin = require('assets-webpack-plugin');
const productionDir = '/frontend/random-lore-generator/';
module.exports = {
 configureWebpack: {
     plugins: [
         new AssetsPlugin({filename: 'dist/assets.json'})
     ]
 },
 publicPath: process.env.NODE_ENV === 'production' ? productionDir + '/dist' : '/',
};
