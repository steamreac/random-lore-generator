<?php
namespace Controller;

define('BUNGIE_URL', 'https://www.bungie.net');
define('API_KEY', 'c804e35e24414486bdaab9221bc91518');
// define('USER_AGENT', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1');
define('SETTING_FILE', 'settings/settings.json');
define('DEFAULT_OPTIONS', [
	// CURLOPT_USERAGENT => USER_AGENT,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_SSL_VERIFYHOST => 2,
]);
define('DATABASE', 'world_sql_content_ru.content');

class Manifest {
	public function loadSettings() {
		if (!file_exists(SETTING_FILE)) return new \stdClass();
		return json_decode(file_get_contents(SETTING_FILE));
	}
	public function setSetting($name, $value) {
		$settings = $this->loadSettings();
		$settings->{$name} = $value;
		file_put_contents(SETTING_FILE, json_encode($settings));
	}
	public function getSetting($name) {
		$settings = $this->loadSettings();
		if (isset($settings->{$name})) return $settings->{$name};
		return '';
	}
	
	public function queryManifest($query) {
		$database = DATABASE;
		$cacheFilePath = 'cache/'.pathinfo($database, PATHINFO_BASENAME);
		$results = [];

		if ($db = new \PDO("sqlite:". $cacheFilePath)) {
			$result = $db->query($query);
			if($rows = $result->fetchAll()) {
				foreach($rows as $row) {
					$key = is_numeric($row[0]) ? sprintf('%u', $row[0] & 0xFFFFFFFF) : $row[0];
					$results[$key] = json_decode($row['json']);
				}
			}
		}

		return $results;
	}

	public function getDefinition($tableName) {
		return $this->queryManifest('SELECT * FROM '.$tableName);
	}
	
	public function getSingleDefinition($tableName, $id) {
		$tables = $this->getSetting('tables');
	
		$key = $tables->{$tableName}[0];
		$where = ' WHERE '.(is_numeric($id) ? $key.'= '.$id.' OR '.$key.'='.($id-4294967296) : $key.'= "'.$id.'"');
		$results = $this->queryManifest('SELECT * FROM '.$tableName.$where);
	
		return isset($results[$id]) ? $results[$id] : false;
	}

	public function getLoreCollection() {
		$loreCollection = $this->getDefinition('DestinyLoreDefinition');
		return $loreCollection;
	}

	public function getShuffledKeys() {
		$loreCollection = $this->getLoreCollection();
		$loreKeys = array_keys($loreCollection);
		shuffle($loreKeys);

		header('Content-Type: application/json');
		return $this->jsonResponse($loreKeys);
	}

	public function getSingleLoreItem() {
		$request = $_REQUEST;
		$id = $request['id'];
		
		if ($id == 'null') {
			http_response_code(500);
			die;
		}

		$loreItem = $this->getSingleDefinition('DestinyLoreDefinition', $id);
		return $this->jsonResponse($loreItem);
	}
	
	public function createView() {
		include_once($_SERVER['DOCUMENT_ROOT'] . '/Classes/views/shuffledCollection.php');
	}

	public function test() {
		// phpinfo();
		echo 'НЕ ВЛЕЗАЙ, УБЬЕТ';
	}

	private function jsonResponse($data) {
		header('Content-Type: application/json');
		echo json_encode($data);
		die;
	}
}
