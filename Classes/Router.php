<?php

class Router {
	private $routes = [
        '{^/api/getShuffledKeys/$}' => ['controller' => 'Manifest','action' => 'getShuffledKeys'],
        '{^/api/getSingleLoreItem/$}' => ['controller' => 'Manifest','action' => 'getSingleLoreItem'],
        '{^/test/$}' => ['controller' => 'Manifest','action' => 'test'],
    ];

    public function __construct() {
        $formattedURL = '/' . trim($_REQUEST['url'], '/') . '/';

        foreach($this->routes as $pattern => $params) {
            if (preg_match($pattern, $formattedURL)) {
                $className = 'Controller\\' . $params['controller'];
                $methodName = $params['action'];
                $controller = new $className;
                $controller->$methodName();
            }
        }
    }
}
