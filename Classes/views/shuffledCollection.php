<?php
	$buildsDir = $_SERVER['DOCUMENT_ROOT'] . '/frontend/random-lore-generator/dist';
	$assets = json_decode(file_get_contents($buildsDir . '/assets.json'), true);
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image/png" href="/favicon.ico"/>
		<title>Генератор рандомного лора</title>
		<script src="<?=$assets['app']['js']?>"></script> 
		<script src="<?=$assets['chunk-vendors']['js']?>"></script>
		<link rel="stylesheet" href="<?=$assets['app']['css']?>">
	</head>
	<body>
		<header>
		</header>
		<main>
			<div id='app'>

			</div>
		</main>
		<footer>
		</footer>
	</body>
</html>
