<?php

use Controller\Manifest;

require_once('Classes/Router.php');
require_once('Classes/Controllers/Manifest.php');

new Router;

$manifest = new Manifest();
$manifest->createView();
